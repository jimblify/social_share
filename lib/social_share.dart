import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';

class SocialShare {
  static const MethodChannel _channel = const MethodChannel('social_share');

  static Future<Map> checkInstalledAppsForShare() async =>
      await _channel.invokeMethod('checkInstalledApps');

  static Future shareInstagramStory(Uint8List imageData, String caption,
      String attributionURL, {String filename = 'image.jpeg'}) async {
    var imagePath = await writeToFile(imageData, filename);

    Map<String, dynamic> args = <String, dynamic>{
      "image": imagePath,
      "caption": caption,
      "attributionURL": attributionURL
    };
    return await _channel.invokeMethod('shareInstagramStory', args);
  }

  static Future shareFacebookStory(Uint8List imageData, String caption,
      String attributionURL, {String filename = 'image.jpeg'}) async {
    var imagePath = await writeToFile(imageData, filename);

    Map<String, dynamic> args = <String, dynamic>{
      "image": imagePath,
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod('shareFacebookStory', args);
  }

  static Future shareInstagramFeed(Uint8List imageData, String caption,
      String attributionURL, {String filename = 'image.jpeg'}) async {
    var imagePath = Platform.isIOS ? await GallerySaver.saveImage(
        await writeToFile(imageData, filename)) : await writeToFile(
        imageData, filename);
    Map<String, dynamic> args = <String, dynamic>{
      "image": imagePath,
      "caption": caption,
      "attributionURL": attributionURL
    };
    return await _channel.invokeMethod('shareInstagramFeed', args);
  }

  static Future shareFacebookFeed(Uint8List imageData, String caption,
      String attributionURL, {String filename = 'image.jpeg'}) async {
    var imagePath = await writeToFile(imageData, filename);

    Map<String, dynamic> args = <String, dynamic>{
      "image": imagePath,
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod('shareFacebookFeed', args);
  }

  static Future shareTwitter(String captionText,
      {List<String> hashtags, String url, String trailingText}) async {
    Map<String, dynamic> args;
    String modifiedUrl;
    if (Platform.isAndroid) {
      modifiedUrl = Uri.parse(url).toString().replaceAll('#', "%23");
    } else {
      modifiedUrl = Uri.parse(url).toString();
    }
    if (hashtags != null && hashtags.isNotEmpty) {
      String tags = "";
      hashtags.forEach((f) {
        tags += ("%23" + f.toString() + " ").toString();
      });
      args = <String, dynamic>{
        "captionText":
        Uri.parse(captionText + "\n" + tags.toString()).toString(),
        "url": modifiedUrl,
        "trailingText": Uri.parse(trailingText).toString()
      };
    } else {
      args = <String, dynamic>{
        "captionText": Uri.parse(captionText + " ").toString(),
        "url": modifiedUrl,
        "trailingText": Uri.parse(trailingText).toString()
      };
    }
    return await _channel.invokeMethod('shareTwitter', args);
  }

  static Future shareWhatsAppImage(Uint8List imageData, String caption,
      String attributionURL, {String filename = 'image.jpeg'}) async {
    var imagePath = await writeToFile(imageData, filename);
    Map<String, dynamic> args = <String, dynamic>{
      "image": imagePath,
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod(
        'shareWhatsappImage', args);
  }

  static Future shareWhatsAppText(String caption,
      String subject) async {
    Map<String, dynamic> args = <String, dynamic>{
      "caption": caption,
      "subject": subject,
    };
    return await _channel.invokeMethod(
        'shareWhatsappText', args);
  }

  static Future shareTelegram(String content) async {
    final Map<String, dynamic> args = <String, dynamic>{"content": content};
    return await _channel.invokeMethod('shareTelegram', args);
  }

  static Future shareMessenger(String caption,
      String attributionURL) async {
    Map<String, dynamic> args = <String, dynamic>{
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod('shareMessenger', args);
  }

  static Future<String> writeToFile(Uint8List imageData,
      String filename) async {
    final tempDir = await getTemporaryDirectory();
    final imagePath = '${tempDir.path}/$filename';
    File file = File(imagePath);
    if (await file.exists())
      await file.delete();
    else
      await file.create();
    await file.writeAsBytes(imageData);
    return imagePath;
  }

}
