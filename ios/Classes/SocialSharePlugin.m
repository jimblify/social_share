#import "SocialSharePlugin.h"
#include <objc/runtime.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@implementation SocialSharePlugin {
    FlutterMethodChannel* _channel;
    UIDocumentInteractionController* _dic;
    FlutterResult _result;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"social_share"
                                     binaryMessenger:[registrar messenger]];
    SocialSharePlugin* instance = [[SocialSharePlugin alloc] initWithChannel:channel];
    [registrar addApplicationDelegate:instance];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)initWithChannel:(FlutterMethodChannel*)channel {
    self = [super init];
    if(self) {
        _channel = channel;
    }
    return self;
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:
(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    return [[FBSDKApplicationDelegate sharedInstance]
            application:application
            openURL:url
            sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
            annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    BOOL handled =
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    return handled;
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    _result = result;
    if([@"shareInstagramStory" isEqualToString:call.method]){
        //Sharing story on instagram
        NSString *stickerImage = call.arguments[@"image"];
        NSString *attributionURL = call.arguments[@"attributionURL"];
        //getting image from file
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isFileExist = [fileManager fileExistsAtPath: stickerImage];
        UIImage *imgShare;
        if (isFileExist) {
            //if image exists
            imgShare = [[UIImage alloc] initWithContentsOfFile:stickerImage];
        }
        //url Scheme for instagram story
        NSURL *urlScheme = [NSURL URLWithString:@"instagram-stories://share"];
        //adding data to send to instagram story
        if ([[UIApplication sharedApplication] canOpenURL:urlScheme]) {
            //if instagram is installed and the url can be opened
            //If you dont have a background image
            // Assign background image asset and attribution link URL to pasteboard
            NSArray *pasteboardItems = @[@{@"com.instagram.sharedSticker.backgroundImage" : imgShare,
                                           @"com.instagram.sharedSticker.contentURL" : attributionURL
            }];
            if (@available(iOS 10.0, *)) {
                NSDictionary *pasteboardOptions = @{UIPasteboardOptionExpirationDate : [[NSDate date] dateByAddingTimeInterval:60 * 5]};
                // This call is iOS 10+, can use 'setItems' depending on what versions you support
                [[UIPasteboard generalPasteboard] setItems:pasteboardItems options:pasteboardOptions];
                
                [[UIApplication sharedApplication] openURL:urlScheme options:@{} completionHandler:nil];
                //if success
                result(@"sharing");
            } else {
                result(@"this only supports iOS 10+");
            }
        } else {
            NSString *instagramLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id389801252";
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:instagramLink] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:instagramLink] options:@{} completionHandler:nil];
            }
            result(@"not supported or no instagram installed");
        }
    }
    else if ([@"shareInstagramFeed" isEqualToString:call.method]) {
        //url Scheme for instagram story
        NSURL *urlScheme = [NSURL URLWithString:@"instagram://share"];
        //adding data to send to instagram story
        if ([[UIApplication sharedApplication] canOpenURL:urlScheme]) {
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:urlScheme options:@{} completionHandler:nil];
                result(@"sharing");
            } else {
                result(@"this only supports iOS 10+");
            }
        } else {
            NSString *instagramLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id389801252";
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:instagramLink] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:instagramLink] options:@{} completionHandler:nil];
            }
            result(@"not supported or no instagram installed");
        }
    }  else if ([@"shareFacebookFeed" isEqualToString:call.method]) {
        NSURL *fbURL = [NSURL URLWithString:@"fbapi://"];
        if([[UIApplication sharedApplication] canOpenURL:fbURL]) {
            [self facebookShare:call.arguments[@"image"]];
            result(@"sharing");
        } else {
            NSString *fbLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id284882215";
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            }
            result(@"not supported or no facebook installed");
        }
    }
    else if([@"shareFacebookStory" isEqualToString:call.method]){
        NSString *stickerImage = call.arguments[@"image"];
        NSString *attributionURL = call.arguments[@"attributionURL"];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        NSString *appID = [dict objectForKey:@"FacebookAppID"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isFileExist = [fileManager fileExistsAtPath: stickerImage];
        UIImage *imgShare;
        if (isFileExist) {
            imgShare = [[UIImage alloc] initWithContentsOfFile:stickerImage];
        }
        NSURL *urlScheme = [NSURL URLWithString:@"facebook-stories://share"];
        if ([[UIApplication sharedApplication] canOpenURL:urlScheme]) {
            
            // Assign background image asset and attribution link URL to pasteboard
            NSArray *pasteboardItems = @[@{@"com.facebook.sharedSticker.backgroundImage" : imgShare,
                                           @"com.facebook.sharedSticker.contentURL" : attributionURL,
                                           @"com.facebook.sharedSticker.appID" : appID}];
            if (@available(iOS 10.0, *)) {
                NSDictionary *pasteboardOptions = @{UIPasteboardOptionExpirationDate : [[NSDate date] dateByAddingTimeInterval:60 * 5]};
                // This call is iOS 10+, can use 'setItems' depending on what versions you support
                [[UIPasteboard generalPasteboard] setItems:pasteboardItems options:pasteboardOptions];
                
                [[UIApplication sharedApplication] openURL:urlScheme options:@{} completionHandler:nil];
                result(@"sharing");
            } else {
                result(@"this only supports iOS 10+");
            }
        } else {
            NSString *fbLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id284882215";
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            }
            result(@"not supported or no facebook installed");
        }
    } else if([@"shareTwitter" isEqualToString:call.method]){
        NSString *captionText = call.arguments[@"captionText"];
        NSString *urlstring = call.arguments[@"url"];
        NSString *trailingText = call.arguments[@"trailingText"];
        
        NSString* urlTextEscaped = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString: urlTextEscaped];
        NSURL *urlScheme = [NSURL URLWithString:@"twitter://"];
        if ([[UIApplication sharedApplication] canOpenURL:urlScheme]) {
            //check if twitter app exists
            //check if it contains a link
            if ( [ [url absoluteString]  length] == 0 ){
                NSString *urlSchemeTwitter = [NSString stringWithFormat:@"twitter://post?message=%@",captionText];
                NSURL *urlSchemeSend = [NSURL URLWithString:urlSchemeTwitter];
                if (@available(iOS 10.0, *)) {
                    [[UIApplication sharedApplication] openURL:urlSchemeSend options:@{} completionHandler:nil];
                    result(@"sharing");
                } else {
                    result(@"this only supports iOS 10+");
                }
            }else{
                //check if trailing text equals null
                if ( [ trailingText   length] == 0 ){
                    //if trailing text is null
                    NSString *urlSchemeSms = [NSString stringWithFormat:@"twitter://post?message=%@",captionText];
                    //appending url with normal text and url scheme
                    NSString *urlWithLink = [urlSchemeSms stringByAppendingString:[url absoluteString]];
                    
                    //final urlscheme
                    NSURL *urlSchemeMsg = [NSURL URLWithString:urlWithLink];
                    if (@available(iOS 10.0, *)) {
                        [[UIApplication sharedApplication] openURL:urlSchemeMsg options:@{} completionHandler:nil];
                        result(@"sharing");
                    } else {
                        result(@"this only supports iOS 10+");
                    }
                }else{
                    //if trailing text is not null
                    NSString *urlSchemeSms = [NSString stringWithFormat:@"twitter://post?message=%@",captionText];
                    //appending url with normal text and url scheme
                    NSString *urlWithLink = [urlSchemeSms stringByAppendingString:[url absoluteString]];
                    NSString *finalurl = [urlWithLink stringByAppendingString:trailingText];
                    //final urlscheme
                    NSURL *urlSchemeMsg = [NSURL URLWithString:finalurl];
                    if (@available(iOS 10.0, *)) {
                        [[UIApplication sharedApplication] openURL:urlSchemeMsg options:@{} completionHandler:nil];
                        result(@"sharing");
                    } else {
                        result(@"this only supports iOS 10+");
                    }
                }
            }
            
        }else{
            result(@"cannot find Twitter app");
            
        }
    }else if([@"shareWhatsappImage" isEqualToString:call.method]){
        NSString *image = call.arguments[@"image"];
        NSString *caption = call.arguments[@"caption"];
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
            _dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:image]];
            _dic.UTI = @"net.whatsapp.image";
            UIViewController* controller = [UIApplication sharedApplication].delegate.window.rootViewController;
            [_dic presentOpenInMenuFromRect:CGRectZero inView:controller.view animated: YES];
            result(@"sharing");
        }else {
            result(@"cannot open Whatsapp");
        }
    }else if([@"shareWhatsappText" isEqualToString:call.method]){
        NSString *subject = call.arguments[@"subject"];
        NSString *caption = call.arguments[@"caption"];
        NSString * urlScheme = [NSString stringWithFormat:@"whatsapp://send?text=%@",caption];
        NSURL *whatsappURL = [NSURL URLWithString:[urlScheme stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) { [[UIApplication sharedApplication] openURL: whatsappURL options:@{} completionHandler:nil];
            result(@"sharing");
        }else {
            result(@"cannot open Whatsapp");
        }
    }else if([@"shareTelegram" isEqualToString:call.method]){
        NSString *content = call.arguments[@"content"];
        NSString * urlScheme = [NSString stringWithFormat:@"tg://msg?text=%@",content];
        NSURL * telegramURL = [NSURL URLWithString:[urlScheme stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if ([[UIApplication sharedApplication] canOpenURL: telegramURL]) {
            [[UIApplication sharedApplication] openURL: telegramURL options:@{} completionHandler:nil];
            result(@"sharing");
        } else {
            result(@"cannot open Telegram");
        }
        result([NSNumber numberWithBool:YES]);
    }
    else if([@"shareMessenger" isEqualToString:call.method]){
        NSString *caption = call.arguments[@"caption"];
        NSString *attributionURL = call.arguments[@"attributionURL"];
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        
        content.quote = caption;
        content.contentURL = [NSURL URLWithString:attributionURL];
        if ([[[FBSDKMessageDialog alloc] init] canShow]) {
            [FBSDKMessageDialog showWithContent:content delegate:self];
             result(@"sharing");
        }
        else {
            NSString *fbLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id454638411";
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            
            result(@"not supported or no facebook messenger installed");
        }
    }
    else if([@"checkInstalledApps" isEqualToString:call.method]){
        NSMutableDictionary *installedApps = [[NSMutableDictionary alloc] init];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram-stories://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"instagram"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"instagram"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"facebook-stories://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"facebook"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"facebook"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"twitter"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"twitter"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"whatsapp://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"whatsapp"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"whatsapp"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb-messenger-share-api://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"messenger"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"messenger"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tg://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"telegram"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"telegram"];
        }
        result(installedApps);
    }
    else {
        result(FlutterMethodNotImplemented);
    }
}

- (void)facebookShare:(NSString*)imagePath {
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = [[UIImage alloc] initWithContentsOfFile:imagePath];
    photo.userGenerated = YES;
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    
    UIViewController* controller = [UIApplication sharedApplication].delegate.window.rootViewController;
    [FBSDKShareDialog showFromViewController:controller withContent:content delegate:self];
}

-(UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    [_channel invokeMethod:@"onSuccess" arguments:nil];
    NSLog(@"Sharing completed successfully");
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    [_channel invokeMethod:@"onCancel" arguments:nil];
    NSLog(@"Sharing cancelled");
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    [_channel invokeMethod:@"onError" arguments:nil];
    NSLog(@"%@",error);
}

@end
